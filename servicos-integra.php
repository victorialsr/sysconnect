<?php require_once ("header.php"); ?>

<main class="main-servicos-integra">
    <section class="banner banner-hero" id="card-service_banner-hero-img"> <!--section-hero--> 
        <div class="container-fluid container-banner-hero">
            <nav class="breadcrumb">
                <ul>
                    <li>Você está em</li>
                    <li><a class="breadcrumb-active" href="index.php">Home</a></li>
                    <li><a href="servicos.php" class="breadcrumb-active"> Serviços</a></li>
                </ul>
            </nav>
            <h1 id="card-service_banner-hero-title" class="--servicosIntegra"></h1>
        </div>            
    </section> <!--/section-hero-->
    <section class="section-info-card --servicosIntegra"> <!--section-card-->
        <div class="container container-mobile-fluid container-card --servicosIntegra">
            <div class="card" id="card_service">
            </div>
        </div>
    </section> <!--/section-card-->

    <section class="section-services --servicosIntegra">  
    <div class="container container-services"> 
        <div class="text">        
        <h3 class="subtitle-h3">Gostou? Conheça mais os nossos</h3>
        <h2 class="title-h2">Outros serviços</h2>
        </div>
    <div class="service-slider">
        <div class="swiper-button-next swiper-services-button-next --servicosIntegra"></div>
        <div class="swiper-button-prev swiper-services-button-prev --servicosIntegra"></div>
        <div class="swiper swiper-services --servicosIntegra">
          <div class="swiper-wrapper dynamic-swiper-wrapper">  
        
          </div> 
         </div>
       </div>
      </div>
    </section>
    
    <section class="banner banner-cta --servicosIntegra"> <!--section-banner-cta-->
        <div class="container-fluid container-banner-cta">
            <div class="text-banner-cta --quemSomos"> 
                <h2 class="title-h2-banner --quemSomos">A SOLUÇÃO IDEAL VOCÊ ENCONTRA AQUI!</h2>
            </div>  
            <a href="contato.php" class="btn btn-cta --quemSomos">quero um orçamento gratuito</a>
        </div>
    </section> <!--/section-banner-cta-->
</main>
<?php require_once ("footer.php"); ?>