<?php require_once ("header.php"); ?>

<main class="main-blog-integra">
    <section class="banner banner-hero --blogIntegra" id="card-blog_banner-hero-img"> <!--section-hero--> 
        <div class="container-fluid container-banner-hero">
            <nav class="breadcrumb --blogIntegra">
                <ul>
                    <li>Você está em</li>
                    <li><a class="breadcrumb-active" href="index.php">Home</a></li>
                    <li><a href="blog.php" class="breadcrumb-active"> Blog</a></li>
                </ul>
            </nav>
            <h1 id="card-blog_banner-hero-title" class="--servicosIntegra main-title"></h1>
        </div>            
    </section> <!--/section-hero-->
    <section class="section-info-card --blogIntegra"> <!--section-card-->
        <div class="container container-mobile-fluid container-card --servicosIntegra">
            <div class="card --blog" id="card_blog">
            </div>
        </div>
    </section> <!--/section-card-->

    <section class="section-card-blog --blogIntegra">
        <div class="container container-card-blog --blog">
            <h3 class="subtitle-h3">Leia mais</h3>
            <h2 class="title-h2">Outras notícias</h2>
            <div class="cards-post-blog cards-other-posts">
                
            </div>
        </div>
    </section> 
    
    <section class="banner banner-cta --blog"> <!--section-banner-cta-->
        <div class="container-fluid container-banner-cta">
            <div class="text-banner-cta --quemSomos"> 
                <h2 class="title-h2-banner --quemSomos">A SOLUÇÃO IDEAL VOCÊ ENCONTRA AQUI!</h2>
            </div>  
            <a href="contato.php" class="btn btn-cta --quemSomos">quero um orçamento gratuito</a>
        </div>
    </section> <!--/section-banner-cta-->
</main>
<?php require_once ("footer.php"); ?>