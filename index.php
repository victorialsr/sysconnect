<?php require_once ("header.php"); ?>
  <main>
    <section class="section-hero">
      <div class="swiper swiper-hero">
        <div class="swiper-wrapper">
          <div class="swiper-slide">
            <div class="background-slide-hero background-hero-1">
              <div class="content">
                <h1 class="title title-hero main-title --home">
                  A Sysconnect é especialista em sistemas web
                </h1>
                <p class="text text-hero">
                  Entre em contato conosco e conheça nossas soluções
                  personalizáveis para cada negócio.
                </p>
                <a href="contato.php" class="btn btn-hero">Saiba mais</a>
              </div>
            </div>
          </div>
          <div class="swiper-slide">
            <div class="background-slide-hero background-hero-1">
              <div class="content">
                <h1 class="title title-hero --home">
                  A Sysconnect é especialista em sistemas web
                </h1>
                <p class="text text-hero">
                  Entre em contato conosco e conheça nossas soluções
                  personalizáveis para cada negócio.
                </p>
                <a href="contato.php" class="btn btn-hero">Saiba mais</a>
              </div>
            </div>
          </div>
          <div class="swiper-slide">
            <div class="background-slide-hero background-hero-1">
              <div class="content">
                <h1 class="title title-hero --home">
                  A Sysconnect é especialista em sistemas web
                </h1>
                <p class="text text-hero">
                  Entre em contato conosco e conheça nossas soluções
                  personalizáveis para cada negócio.
                </p>
                <a href="contato.php" class="btn btn-hero">Saiba mais</a>
              </div>
            </div>
          </div>
        </div>
        <div class="swiper-pagination"></div>
      </div>
    </section>

    <section class="section-services --home">
      <div class="container container-services">
        <div class="swiper-button-next swiper-services-button-next --home"></div>
        <div class="swiper-button-prev swiper-services-button-prev --home"></div>
        <div class="swiper swiper-services --home">
          <div class="swiper-wrapper">
            <div class="swiper-slide slide-1">
              <div class="card-services card-1">
                <div class="card-content --home">
                  <h3>Suporte e manutenção</h3>
                  <p>Atendimento técnico especializado ágil e eficaz. Auxílio também para alimentação de informações.
                  </p>
                  <a href="servicos-integra.php?slug=suporte-e-manutencao">> saber mais</a>
                </div>
                <div class="card-image card-image-1"></div>
              </div>
            </div>
            <div class="swiper-slide slide-2">
              <div class="card-services card-2">
                <div class="card-content --home">
                  <h3>Criação de Sites</h3>
                  <p>Sites responsivos, com navegação intuitiva e design profissional. Sites com foco em geração de resultados.
                  </p>
                  <a href="servicos-integra.php?slug=criacao-de-sites">> saber mais</a>
                </div>
                <div class="card-image card-image-2"></div>
              </div>
            </div>
            <div class="swiper-slide slide-3">
              <div class="card-services card-3">
                <div class="card-content --home">
                  <h3>Sistemas em Nuvem</h3>
                  <p>
                  Software acessível pela web, desenvolvido sob-medida, com segurança de datacenter profissional
                  </p>
                  <a href="servicos-integra.php?slug=sistemas-em-nuvem">> saber mais</a>
                </div>
                <div class="card-image card-image-3"></div>
              </div>
            </div>

            <div class="swiper-slide slide-4">
              <div class="card-services card-4">
                <div class="card-content --home">
                  <h3>E-commerce</h3>
                  <p>
                  Criação de lojas virtuais com gestão de pedidos, integração com cartões de crédito, boletos, etc.
                  </p>
                  <a href="servicos-integra.php?slug=ecommerce">> saber mais</a>
                </div>
                <div class="card-image card-image-4"></div>
              </div>
            </div>

            <div class="swiper-slide slide-5">
              <div class="card-services card-5">
                <div class="card-content --home">
                  <h3>SEO </h3>
                  <p>
                  Otimização de sites para destaque no Google. Resultados orgânicos, gerando tráfego e autoridade para o seu site.
                  </p>
                  <a href="servicos-integra.php?slug=seo">> saber mais</a>
                </div>
                <div class="card-image card-image-5"></div>
              </div>
            </div>

            <div class="swiper-slide slide-6">
              <div class="card-services card-6">
                <div class="card-content --home">
                  <h3>Intranet e Extranet </h3>
                  <p>
                  Equipe integrada em um único ambiente, controle de processos e maior produtividade.
                  </p>
                  <a href="servicos-integra.php?slug=intranet-e-extranet">> saber mais</a>
                </div>
                <div class="card-image card-image-6"></div>
              </div>
            </div>

            <div class="swiper-slide slide-7">
              <div class="card-services card-7">
                <div class="card-content --home">
                  <h3>Google Adwords</h3>
                  <p>
                  Campanhas Google Ads, planejamento estratégico e análise de palavras-chave. Relatório mensal de performance.
                  </p>
                  <a href="servicos-integra.php?slug=google-adwords">> saber mais</a>
                </div>
                <div class="card-image card-image-7"></div>
              </div>
            </div>

            <div class="swiper-slide slide-8">
              <div class="card-services card-8">
                <div class="card-content --home">
                  <h3>Criação de App’s </h3>
                  <p>Criação de aplicativos para Iphone e Android. Desenvolvimento de aplicativos sob medida.
                  </p>
                  <a href="servicos-integra.php?slug=criacao-de-apps">> saber mais</a>
                </div>
                <div class="card-image card-image-8"></div>
              </div>
            </div>

            <div class="swiper-slide slide-9">
              <div class="card-services card-9">
                <div class="card-content --home">
                  <h3>Marketing de Conteúdo</h3>
                  <p>
                  Produção de conteúdo para Blogs, com foco em atração de visitantes através do Google. Posts 100% otimizados.
                  </p>
                  <a href="servicos-integra.php?slug=marketing-de-conteudo">> saber mais</a>
                </div>
                <div class="card-image card-image-9"></div>
              </div>
            </div>
          </div> 
         </div>
      </div>
    </section>

    <section class="section-costumers">
      <h2 class="title-costumers">Conheça nossos clientes</h2>
      <div class="container-costumers">
        <div class="swiper swiper-costumers">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <div class="card-costumers">
                <a href="https://www.samsung.com/br/">
                  <img src="./assets/img/samsung-logo.png" alt="Logo da empresa Samsung" />
                </a>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="card-costumers">
                <a href="https://www.kbrtec.com.br">
                  <img src="./assets/img/kbrtec-logo.png" alt="Logo da empresa KBR Tec" />
                </a>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="card-costumers">
                <a href="https://www.capital.sp.gov.br">
                  <img src="./assets/img/prefeiturasp-logo.png" alt="Brasão da prefeitura da cidade de São Paulo" />
                </a>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="card-costumers">
                <a href="https://www.philips.com.br">
                  <img src="./assets/img/phillips-logo.png" alt="Logo da empresa Phillips" />
                </a>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="card-costumers">
                <a href="https://www.mktvirtual.com.br">
                  <img src="./assets/img/mktvirtual-logo.png" alt="Logo da empresa Marketing Virtual" />
                </a>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="card-costumers">
                <a href="http://www.itautec.com/">
                  <img src="./assets/img/itautec-logo.png" alt="Logo da empresa Itaú Tec" />
                </a>
              </div>
            </div>
          </div>
        </div>  
        <div class="swiper-button-next swiper-costumers-button-next"></div>
        <div class="swiper-button-prev swiper-costumers-button-prev"></div>
      </div> 
    </section>

    <section class="section-blog">
      <div class="container container-blog">
        <div class="col-sysconnect">
          <h2 class="title">A Sysconnect</h2>
          <div class="card-blog">
            <a href="quem-somos.php">
              <img src="./assets/img/img-blog-1.jpg" alt="Imagem de uma pessoa digitando no teclado de um notebook" />
            </a>
            <a href="quem-somos.php">            
              <p class="paragraph-sysconnect">
              Lorem ipsum dolor sit amet, consectetur elit ornare tempus. Duis
              ornare elit vitae velit tempus, nec.
              </p>
            </a>
          </div>
        </div>
        <div class="col-blog">
          <h2 class="title">Blog</h2>
          <div class="card-blog">
            <a href="blog-integra.php?post=por-que-meu-blog-nao-consegue-aparecer-no-google">
              <img src="./assets/img/img-blog-2.jpg" alt="Imagem de um homem de óculos segurando um notebook" />
            </a>
            <a href="blog-integra.php?post=por-que-meu-blog-nao-consegue-aparecer-no-google">
              <h3 class="heading-3-blog">Destaque</h3>
            </a>
            <a href="blog-integra.php?post=por-que-meu-blog-nao-consegue-aparecer-no-google">
              <p class="paragraph-blog-1">
                Lorem ipsum dolor sit amet, consectetur elit ornare tempus. Duis
                ornare elit vitae velit tempus, nec.
              </p>
            </a>
            <a href="blog-integra.php?post=por-que-meu-blog-nao-consegue-aparecer-no-google" class="read-more">> ler mais</a>
          </div>
        </div>
        <div class="col-blog">
          <div class="card-blog-2">
            <a href="blog-integra.php?post=desenvolvimento-de-apps-quanto-custa-uma-aplicacao">
              <p class="paragraph-blog-2">
                Lorem ipsum dolor sit amet, consectetur elit ornare tempus. Duis
                ornare elit vitae velit tempus, nec.
              </p>
            </a>
            <a href="blog-integra.php?post=desenvolvimento-de-apps-quanto-custa-uma-aplicacao" class="read-more">> ler mais</a>
            <a href="blog-integra.php?post=wordpress-drupal-shopify-joomla-ou-wix-qual-e-a-melhor-plataforma-para-criacao-de-conteudo">
              <p class="paragraph-blog-2">
                Lorem ipsum dolor sit amet, consectetur elit ornare tempus. Duis
                ornare elit vitae velit tempus, nec.
              </p>
            </a>
            <a href="blog-integra.php?post=wordpress-drupal-shopify-joomla-ou-wix-qual-e-a-melhor-plataforma-para-criacao-de-conteudo" class="read-more">> ler mais</a>
            <a href="blog-integra.php?post=responsividade-como-e-porque-ela-e-importante-para-o-seu-site-ou-portal">
              <p class="paragraph-blog-2">
                Lorem ipsum dolor sit amet, consectetur elit ornare tempus. Duis
                ornare elit vitae velit tempus, nec.
              </p>
            </a>
            <a href="blog-integra.php?post=responsividade-como-e-porque-ela-e-importante-para-o-seu-site-ou-portal" class="read-more">> ler mais</a>
          </div>
        </div>
      </div>
    </section>
  </main>
<?php require_once ("footer.php"); ?>
