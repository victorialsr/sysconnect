var cards = [
  {
    id: 1,
    title: "Suporte e manutenção",
    description:
      "Atendimento técnico especializado ágil e eficaz. Auxílio também para alimentação de informações.",
    image: "assets/img/img-card-1.png",
    singleHeroBanner: "assets/img/banner-hero_suporte-e-manutencao.png",
    singleDescription: `<p class="paragraph-card --servicosIntegra">Oferecemos <strong>manutenção de sites</strong> de todos os tipos, com atendimento técnico especializado, ágil e eficaz. A melhor maneira de obter sucesso na web é ter um projeto evolutivo e programado.</p> 
        <p class="paragraph-card --servicosIntegra">Justamente em função disso, é muito útil o acompanhamento permanente de técnicos especializados. Nosso pacote de manutenção de sites básico conta com uma gama de 10 serviços, que vão desde a aprimoramentos de design, atendimento a emergências, aperfeiçoamentos tecnológicos, criação de novos serviços aos usuários, atendimento de chamados para correções, atualizações e adaptações.</p> 
        <p class="paragraph-card --servicosIntegra">Caso seja interessante, podemos acrescentar também aos serviços de manutenção de sites o auxílio para alimentação de fotos, vídeos, textos e artes gráficas, assim você não precisa se preocupa com nada. O serviço de manutenção permanente de seu site evita a obsolescência e otimiza o seu investimento. Site sempre moderno e bem atualizado. <a href="contato.php">Entre em contato conosco!</a></p>`,
    singleImage: "assets/img/bg-card_suporte-e-manutencao.png",
    slug: "suporte-e-manutencao",
  },
  {
    id: 2,
    title: "Criação de Sites",
    description:
      "Sites responsivos, com navegação intuitiva e design profissional. Sites com foco em geração de resultados.",
    image: "assets/img/img-card-2.png",
    singleHeroBanner: "assets/img/banner-hero_criacao-de-sites.png",
    singleDescription: `<p class="paragraph-card --servicosIntegra"> A <strong>criação de sites</strong> pode parecer algo simples, um produto commodity, mas isto é um grande engano. Uma coisa é criar um site institucional comum, que possui baixíssimo volume de acessos, outra coisa é a criação de sites profissionais, com design moderno, tecnologia de ponta e alta performance comercial. Fazer um site liderar é um grande desafio e podemos te ajudar</p>
        <p class="paragraph-card --servicosIntegra">Se não sabe como criar um site que faça a diferença, que se destaque no Google, que entregue grande volume de acessos, vendas, consultas e leads, então você deve nos consultar</p> 
        <p class="paragraph-card --servicosIntegra">A Sysconnect realiza a criação de sites responsivos, totalmente adaptados a smartphones e tablets. Isso é muito importante pois atualmente a maior parte dos acessos ocorre através de dispositivos móveis. Um site profissional precisa estar adaptado a essa realidade.</p>
        <p class="paragraph-card --servicosIntegra">Já no que se refere à usabilidade, realizamos a produção de sites com navegação intuitiva e com forte utilização de chamadas para ação ( CTA's ), com foco em resultados comerciais. Realizamos testes A/B para entender o comportamento dos visitantes e com isso aumentamos a sua conversão.</p>
        <p class="paragraph-card --servicosIntegra"><a href='servicos-integra.php?slug=seo' title='Otimizamos sites para destaque no google, conheça esse nosso serviço'>Sites otimizados para o Google ( SEO )</a>, com as <a href='servicos-integra.php?slug=google-adwords' title='Realizamos campanhas com análise de palavras-chave'>palavras-chave</a> ideais para alavancar os seus acessos orgânicos. Isso faz com que seu site tenha uma posição de destaque na 1a página de busca e diminua a necessidade de anúncios pagos, através do Google Ads.</p>
        <p class="paragraph-card --servicosIntegra">Todos os nossos projetos são realizados sob-medida. Antes de iniciarmos um projeto analisamos as suas necessidades e propomos um a criação de um site com as ferramentas e tecnologias certas para alcançar resultados desejados.</p> 
        <p class="paragraph-card --servicosIntegra">Já desenvolvemos 800 projetos sob-medida, com atenção máxima em entregar resultados reais e através de ótimo custo x benefício. Entre em contato conosco. Você vai se surpreender!"</p>`,
    singleImage: "assets/img/bg-card_criacao-de-sites.png",
    slug: "criacao-de-sites",
  },
  {
    id: 3,
    title: "Sistemas em Nuvem",
    description:
      "Software acessível pela web, desenvolvido sob-medida, com segurança de datacenter profissional.",
    image: "assets/img/img-card-3.png",
    singleHeroBanner: "assets/img/banner-hero_sistemas-em-nuvem.png",
    singleDescription:
      '<p class="paragraph-card --servicosIntegra">Os <strong>softwares em nuvem</strong> chegaram para ficar. A possibilidade de disponibilizar sistemas via web permite diversos benefícios incríveis, possibilitando mobilidade e muita facilidades de manutenção. A <strong>Sysconnect desenvolve sistemas em nuvem sob-medida</strong> para uma seleta carteira de clientes</p> <p class="paragraph-card --servicosIntegra">Basta uma conexão com a internet e você acessa seu sistema em nuvem em um hotel, em casa, em uma filial, no seu cliente, em qualquer lugar. O sistema em nuvem pode ser acessado mesmo a partir de um tablet ou celular</p> <p class="paragraph-card --servicosIntegra">A infraestrutura é livre e segura, com uso de <strong>Data Center</strong> de classe mundial, como por exemplo <strong>Amazon (AWS)</strong>. Não precisa instalar nada no seu computador de trabalho, basta um navegador de internet e você tem independência de Sistema Operacional. Mesmo que a sua rede local seja contaminada por vírus, os seus dados ficam seguros e centralizados em nuvem, sem serem afetados.</p> <p class="paragraph-card --servicosIntegra">Não se preocupe mais com rotinas de backup ou qualquer outra preocupação com a infraestrutura, pois o conceito de Cloud Computing chegou para libertar as empresas destas preocupações.</p> <p class="paragraph-card --servicosIntegra">Quando houver necessidade de alterar ou atualizar o seu sistema em nuvem, isso ocorrerá apenas nos servidores - com total segurança. Quando a alteração estiver aprovada, entra em produção e você a acessa sem necessidade de re-instalar nada em seu computador. </p> <p class="paragraph-card --servicosIntegra"> Sistema em nuvem permite colaboração utilizando workflow. envio de e-mails de alertas automáticos, SMS´s automáticos, equipes de trabalho à distância, interações automatizadas com os seus clientes, permitindo inclusive o acompanhamento das atividades em tempo real, se for de seu interesse.</p> <p class="paragraph-card --servicosIntegra">Temos muita experiência e já desenvolvemos muitos sistemas em nuvem para clientes de todos os portes. Utilizamos tecnologias de ponta associadas a metodologias ágeis, que conferem grande assertividade em cronogramas. Cumprimos rigorosamente os prazos para o desenvolvimento de softwares em nuvem, sem surpresas desagradáveis.</p> <p class="paragraph-card --servicosIntegra"><a href="contato.php">Consulte-nos</a> sobre como podemos ajudá-lo com a criação de um sistema em nuvem para a sua empresa.</p>',
    singleImage: "assets/img/bg-card_sistemas-em-nuvem.png",
    slug: "sistemas-em-nuvem",
  },
  {
    id: 4,
    title: "E-commerce",
    description:
      "Criação de lojas virtuais com gestão de pedidos, integração com cartões de crédito, boletos, etc.",
    image: "assets/img/img-card-4.png",
    singleHeroBanner: "assets/img/banner-hero_ecommerce.png",
    singleDescription: `<p class="paragraph-card --servicosIntegra"><strong>Criação de loja virtual</strong> com alta performance, com gestão de pedidos, integração com cartões de crédito, boletos, etc. </p>
        <p class="paragraph-card --servicosIntegra"> Oferecemos lojas virtuais completas, que possuem interface rica e amigável, com ótimo gerenciamento de produtos, clientes e vendas. Possuem ferramentas flexíveis para que você possa criar diversas promoções e sistemas para fidelização de clientes. </p>
        <p class="paragraph-card --servicosIntegra"> Nosso e-commerce já conta com uma vasta gama de formas de pagamentos e permite também customização de design, porém, dentro de alguns parâmetros. O sistema administrativo da loja virtual oferece relatórios estatísticos completos, para que você possa acompanhar de perto o andamento do seu negócio. </p>
        <p class="paragraph-card --servicosIntegra"> Oferecemos ainda consultoria em e-commerce para que possa dar os primeiros passos com assertividade, economizando recursos na etapa de criação da loja virtual.</p>
        <p class="paragraph-card --servicosIntegra"> Agende uma apresentação, <a href='contato.php'>faça uma consulta</a> e venha tomar um café conosco.</p>`,
    singleImage: "assets/img/bg-card_ecommerce.png",
    slug: "ecommerce",
  },
  {
    id: 5,
    title: "SEO",
    description:
      "Otimização de sites para destaque no Google. Resultados orgânicos, gerando tráfego e autoridade para o seu site.",
    image: "assets/img/img-card-5.png",
    singleHeroBanner: "assets/img/banner-hero_seo.png",
    singleDescription: `<p class="paragraph-card --servicosIntegra">A cada dia que passa a otimização de sites vem se tornando mais importante para qualquer negócio. Se existe algo que 100% das empresas desejam é que o site seja um instrumento de vendas ativo. Na verdade ele pode ser sim e a palavra mágica para isso é: <strong>destaque no Google</strong>.</p>
        <p class="paragraph-card --servicosIntegra"> Se um site está bem otimizado no Google, posicionado na primeira página, então ele está visível àqueles que procuram seus produtos ou serviços. </p>
        <p class="paragraph-card --servicosIntegra"><strong>Otimização de sites</strong> para o Google é uma arte que depende de um especialista em SEO. Este não é um trabalho para amador. Atualmente o buscador leva em consideração aproximadamente 200 critérios, que vão desde a velocidade do site, a forma como o código do site foi escrito, se o site é responsivo ou não, se o conteúdo é rico, se o site utiliza dados estruturados, se o site conta com links importantes apontando para ele ( linkbuiding ), se o domínio possui boa autoridade, se o código foi escrito utilizando as tags corretas, se existe ou não a presença de um sitemap, entre muitos outros fatores. 
        <p class="paragraph-card --servicosIntegra">O fato é que ter um site otimizado é um diferencial importante de mercado, pois o número de visitantes aumenta exponencialmente e com isso, o número de vendas e contatos gerados a partir do site. Em nossos projetos de otimização de sites acompanhamos de perto a evolução dos acessos, a evolução de conversões do site e diversas outras métricas importantes para o seu negócio. </p>
        <p class="paragraph-card --servicosIntegra"> Técnicas bem aplicadas de otimização de sites (SEO) são essenciais para gerar resultados em tráfego orgânico - sem que sejam necessários anúncios pagos. Isso faz com que a autoridade do site aumente dia após dia e os resultados do site no Google se tornam ainda melhores.</p>
        <p class="paragraph-card --servicosIntegra">Potencialize as oportunidades comerciais que a internet pode gerar ao seu negócio com a otimização de sites para o Google (SEO). <a href='contato.php'>Consulte-nos!</a></p>`,
    singleImage: "assets/img/bg-card_seo.png",
    slug: "seo",
  },
  {
    id: 6,
    title: "Intranet e Extranet",
    description:
      "Equipe integrada em um único ambiente, controle de processos e maior produtividade.",
    image: "assets/img/img-card-6.png",
    singleHeroBanner: "assets/img/banner-hero_intranet-e-extranet.png",
    singleDescription: `<p class="paragraph-card --servicosIntegra">Que tal ter uma <strong>intranet ou extranet</strong> em sua empresa? As vantagens são diversas, tais como: ter equipe integrada em um único ambiente, distribuição de informações facilitada, controle de processos e maior produtividade.</p>
        <p class="paragraph-card --servicosIntegra"> A Sysconnect desenvolve intranets e extranets com projetos desenvolvidos sob-medida, de acordo com o seu negócio. É possível customização das ferramentas, e escolher de forma modular aquelas que são mais importantes para a sua empresa.</p>
        <p class="paragraph-card --servicosIntegra"> Uma intranet colaborativa bem projetada pode ser a ferramenta que falta em sua empresa. As vantagens são inúmeras: <br><ul><li>Compartilhamento de informações e documentos;</li><li>Redução no uso de papel;</li><li>Facilidade de acesso à informação;</li><li>Controle da produtividade;</li><li>Interatividade entre funcionários;</li><li>Difusão da cultura da sua empresa;</li><li>Trabalho colaborativo</li></ul></p><br><br>
        <p class="paragraph-card --servicosIntegra"> Podemos oferecer uma intranet corporativa flexível e muito simples de ser usada.</p>
        <p class="paragraph-card --servicosIntegra"><a href='contato.php'>Entre em contato conosco!</a></p>`,
    singleImage: "assets/img/bg-card_intranet-e-extranet.png",
    slug: "intranet-e-extranet",
  },
  {
    id: 7,
    title: "Google Adwords",
    description:
      "Campanhas Google Ads, planejamento estratégico e análise de palavras-chave. Relatório mensal de performance.",
    image: "assets/img/img-card-7.png",
    singleHeroBanner: "assets/img/banner-hero_google-adwords.png",
    singleDescription: `<p class="paragraph-card --servicosIntegra">Alguns setores comerciais são muito disputados no Google. Nestes casos, o ideal é pensar em dar um “empurrãozinho” no seu posicionamento através dos links patrocinados <strong>(Google Adwords)</strong></p>
        <p class="paragraph-card --servicosIntegra"> A primeira coisa que passa pela cabeça das pessoas é: vou gastar uma fortuna com anúncio no google para ficar na 1ª página, mas isso não é verdade</p>
        <p class="paragraph-card --servicosIntegra"> Uma campanha <strong>Google Ads</strong> bem planejada é mais barata do que imagina. Você precisa contar com especialistas google adwords para gerir de modo profissional a sua campanha. A Sysconnect pode encontrar nichos de mercado a serem explorados, palavras-chave com alto potencial e menor concorrência, combinar termos específicos ( long tail ) com localizações geográficas e características do seu público alvo. Entregamos bons resultados com baixo orçamento</p>
        <p class="paragraph-card --servicosIntegra"> <a href='contato.php'>Entre em contato conosco.</a> Podemos ajudá-lo a encontrar a melhor relação custo x benefício nos anúncios Google Adwords.</p>`,
    singleImage: "assets/img/bg-card_google-adwords.png",
    slug: "google-adwords",
  },
  {
    id: 8,
    title: "Criação de App's",
    description:
      "Criação de aplicativos para Iphone e Android. Desenvolvimento de aplicativos sob medida.",
    image: "assets/img/img-card-8.png",
    singleHeroBanner: "assets/img/banner-hero_criacao-de-apps.png",
    singleDescription: `<p class="paragraph-card --servicosIntegra">Atualmente as pessoas não desgrudam de seus celulares, não é mesmo? Os smartphones funcionam como uma central de entretenimento para uso pessoal e um verdadeiro computador de bolso para o mundo do trabalho. Goste ou não, o smartphone faz parte do cotidiano de todos nós e a criação de aplicativos pode sim ajudar nos seus negócios.</p>
        <p class="paragraph-card --servicosIntegra">  Que tal oferecer aos seus clientes um <strong>aplicativo moderno e prático</strong> para facilitar a vida deles?</p> 
        <p class="paragraph-card --servicosIntegra"> A Sysconnect oferece serviço de <strong>criação de aplicativos diferenciados para a plataforma Iphone (IOS) e Android</strong> e é mais barato e fácil do que você imagina.</p>
        <p class="paragraph-card --servicosIntegra"> As empresas que puderem criar apps e oferecer conveniência aos seus clientes, com certeza terão um grande diferencial competitivo. Permitir consulta de informações, agilizar um processo que depende de diversos passos, facilitar o processo de comunicação, entre muitas outras facilidades.</p>
        <p class="paragraph-card --servicosIntegra">A criação de aplicativos na Sysconnect utiliza Node JS e React - uma plataforma de desenvolvimento de aplicativos moderna e especialmente interessante a quem deseja criar apps para os dois sistemas ( IOS e Android ). O desenvolvimento de aplicativos é feito sob medida, deste modo entendemos sua necessidade e propomos a criação de aplicativos personalizados, com design exclusivo e plenamente de acordo com as suas necessidades.</p>
        <p class="paragraph-card --servicosIntegra"> Saiba como dar os primeiros passos para criar um aplicativo. Consulte a Sysconnect!</p>`,
    singleImage: "assets/img/bg-card_criacao-de-apps.png",
    slug: "criacao-de-apps",
  },
  {
    id: 9,
    title: "Marketing de Conteúdo",
    description:
      "Produção de conteúdo para Blogs, com foco em atração de visitantes através do Google. Posts 100% otimizados.",
    image: "assets/img/img-card-9.png",
    singleHeroBanner: "assets/img/banner-hero_marketing-de-conteudo.png",
    singleDescription: `<p class="paragraph-card --servicosIntegra"><strong>Marketing de Conteúdo</strong> pode ser entendido como a produção de informações relevantes objetivando trazer o público-alvo desejado ao seu site. Trata-se de uma forma de atrair e envolver as pessoas certas, entregando informações importantes para que decidam comprar o seu produto ou serviço.</p>
        <p class="paragraph-card --servicosIntegra"> Mas não basta escrever um tratado sobre os seus produtos e disponibilizar na web. Na verdade, em Marketing de Conteúdo a forma como o texto é escrito e a <a href='servicos-integra.php?slug=google-adwords'>escolha das palavras-chaves</a> corretas podem ser até mais importantes do que o volume de informações em si. É necessário conhecer todas as técnicas de otimização on-page e é preciso entender como fisgar o visitante do site com CTA's ( chamadas para ação ) realmente eficientes. </p>
        <p class="paragraph-card --servicosIntegra"> Se você deseja atrair visitantes através de posts de um blog, é fundamental contar com especialistas em Marketing de Conteúdo. A Sysconnect tem ótimos cases de sucesso e pode apresentar a você.</p>
        <p class="paragraph-card --servicosIntegra"> Conte com a nossa experiência para planejar uma campanha de marketing de conteúdo vencedora, criando personas, as pautas, produzindo conteúdos de alta performance para o seu blog e apresentando os resultados das ações.</p>
        <p class="paragraph-card --servicosIntegra"> Um post bem posicionado no Google pode significar bons negócios por muito tempo e o que é melhor: uma vez que ele esteja no topo das buscas não é necessário gastar mais nenhum centavo. Ele se transforma em uma máquina de fazer negócios. Pense nisso.</p>
        <p class="paragraph-card --servicosIntegra">Temos uma equipe especializada em Marketing de Conteúdo para entregar os melhores resultados à sua empresa. Faça uma consulta!</p>`,
    singleImage: "assets/img/bg-card_marketing-de-conteudo.png",
    slug: "marketing-de-conteudo",
  },
];

function createCards() {
  try {
    const grid = document.querySelector(".grid");
    grid.innerHTML = cards
      .map(
        (card) => `
        <div class="cell">
            <div class="card-content --servicos">
                <h3 class="--servicos">${card.title}</h3>
                <p class="--servicos">${card.description}</p>
                <a href="servicos-integra.php?slug=${card.slug}">&gt; saber mais</a>
            </div>
            <div class="card-image" style="background-image: url(${card.image})"></div>
        </div>`
      )
      .join("");
  } catch (err) {
    console.log(err);
  }
}
const slug = new URLSearchParams(document.location.search);

const cardServiceSlugFiltered = cards.filter(
  (card) => card.slug == slug.get("slug")
);

function createServicePage() {
  try {
    const cardServiceBannerHeroImg = document.querySelector(
      "#card-service_banner-hero-img"
    );

    const cardServiceBannerHeroTitle = document.querySelector(
      "#card-service_banner-hero-title"
    );

    const cardService = document.querySelector("#card_service");

    cardServiceBannerHeroTitle.innerText = cardServiceSlugFiltered.map(
      (cardFiltered) => `${cardFiltered.title}`
    );
    cardServiceBannerHeroImg.style.backgroundImage =
      cardServiceSlugFiltered.map(
        (cardFiltered) => `url("${cardFiltered.singleHeroBanner}")`
      );

    cardService.innerHTML = cardServiceSlugFiltered.map(
      (cardFiltered) => `
        <div class="info-card --servicosIntegra"> 
            <div class="texto-card">
                ${cardFiltered.singleDescription}
            </div>
            <a href="contato.php" class="btn btn-card --servicosIntegra">Faça já seu orçamento!</a>
        </div>
        <div class="photo-card --servicosIntegra" style="background-image: url(${cardFiltered.singleImage})"> <!--photo-card-->
        </div> <!--/photo-card-->
    `
    );
  } catch (err) {
    console.log(err);
  }
}

function createServiceSlider() {
  try {
    const servicosIntegraSlider = document.querySelector(
      ".dynamic-swiper-wrapper"
    );
    servicosIntegraSlider.innerHTML = cards
      .map((card) =>
        card.slug == slug.get("slug")
          ? ""
          : `
            <div class="swiper-slide">
                <div class="card-services">
                    <div class="card-content --servicosIntegra">
                        <h3 class="--servicos">${card.title}</h3>
                        <p class="--servicos">${card.description}</p>
                        <a href="servicos-integra.php?slug=${card.slug}">&gt; saber mais</a>
                    </div>
                    <div class="card-image" style="background-image: url(${card.image})"></div>
                </div>
            </div>
`
      )
      .join("");
  } catch (err) {
    console.log(err);
  }
}

createCards();
createServicePage();
createServiceSlider();
