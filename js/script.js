var swiper_hero = new Swiper(".swiper-hero", {
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
});


var swiper_services = new Swiper(".swiper-services", {
  slidesPerView: 3,
  slidesPerGroup: 3,
  spaceBetween: 18, 
  loop: true,
  navigation: {
    nextEl: ".swiper-services-button-next",
    prevEl: ".swiper-services-button-prev",
  },
  breakpoints: {
    300: {
      slidesPerView: 1,
      spaceBetween: 40,
      slidesPerGroup: 1,
    },
    800: {
      slidesPerView: 2,
      spaceBetween: 20,
      slidesPerGroup: 1,
    },
    1208: {
      slidesPerView: 3,
      spaceBetween: 18,
      slidesPerGroup: 1,
    },
  },
});      

var swiper_costumers = new Swiper(".swiper-costumers", {
  slidesPerView: 6,
  slidesPerGroup: 6,
  spaceBetween: 15, 
  loop: true,
  navigation: {
    nextEl: ".swiper-costumers-button-next",
    prevEl: ".swiper-costumers-button-prev",
  },
  breakpoints: {
    300: {
      slidesPerView: 1,
      spaceBetween: 40,
      slidesPerGroup: 1,
    },
    600: {
      slidesPerView: 2,
      spaceBetween: 40,
      slidesPerGroup: 1,
    },
    850: {
      slidesPerView: 4,
      spaceBetween: 40,
      slidesPerGroup: 4,
    },
    1000: {
      slidesPerView: 5,
      spaceBetween: 18,
      slidesPerGroup: 1,
    },
    1300: {
      slidesPerView: 6,
      spaceBetween: 18,
      slidesPerGroup: 1,
    },
  },
 
});

/*hamburguer menu*/
const mobileBtn = document.querySelector('#btn-mobile');

function toggleMenu() {
  const nav = document.querySelector('.menu');

  nav.classList.toggle('active');
}

mobileBtn.addEventListener('click', toggleMenu);
/*hamburguer menu*/