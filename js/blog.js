var textoApp =
  '<p class="paragraph-card --blogIntegra">Os diversos aplicativos existentes e as facilidades que eles podem trazer para os usuários, fizeram com que diversos investidores e empreendedores começassem a olhá-los como uma possível fonte de renda, além da criação de uma inovação capaz de diversificar e facilitar a vida das pessoas. Mas quanto custa um aplicativo?</p> <p class="paragraph-card --blogIntegra">Talvez essa seja a primeira pergunta que os futuros criadores fazem quando a ideia surge. Para que essa resposta seja dada de forma assertiva, será necessário entender como funcionam as empresas responsáveis pela criação de app e quais são os processos de produção.</p> <p class="paragraph-card --blogIntegra">Se você tem uma ideia genial e quer colocá-la em prática, você precisa conhecer todos os passos e preços para a realização, por isso o presente artigo visa sanar todas as dúvidas existentes dentro desse universo.</p> <h2 class="subtitle-card --blogIntegra">Qualquer pessoa pode criar um App? </h2><p class="paragraph-card --blogIntegra">Seguindo com a necessidade da contratação de um profissional ou empresa para o desenvolvimento do APP, a escolha entre os tipos existentes também deverá ser feita por quem entende da área. Após a ideia ser concretizada, os técnicos irão fazer uma análise, juntamente com o idealizador, para entender qual será a finalidade do aplicativo e também escolher o seu tipo.</p> <p class="paragraph-card no-space --blog">Hoje, no mercado, pode-se encontrar três tipos principais, sendo um fator determinante na hora da precificação do mesmo. Veja: <p class="paragraph-card --blog"><strong>- Nativo:</strong> APP Nativo leva vantagem pela sua função de trabalho no modo offline e também pela capacidade de aproveitamento das diversas funções existentes nos celulares e tablets. Eles são instalados, interiormente, nos dispositivos e seus códigos podem ser facilmente modificados. A principal desvantagem são os preços elevados que eles podem ter em relação aos outros tipos</p> <p class="paragraph-card --blog"><strong>- Web App: </strong> O aplicativo Web App pode ser apresentado como uma página que representa as funções de alguns aplicativos existentes nos dispositivos móveis. Para que o usuário consiga o acesso, basta ele utilizar a URL correta, onde apenas uma simples instalação possa ser pedida. A sua principal vantagem é a facilidade de utilização e também o preço, que apresenta valor abaixo quando comparado os outros tipos. <p class="paragraph-card --blog"><strong>- Híbridos:</strong> Por possuir um desenvolvimento incompleto nas linguagens específicas dos sistemas operacionais, ele pode ser apresentado em dois formatos: uma parte nativa e outra web App, então a sua utilização pode ser feita tanto pelo computador quanto pelo celular ou tablet. Esse tipo é bastante procurado devido a sua dupla função e também pelo baixo custo. <h2 class="subtitle-card --blogIntegra">Vantagens de ter um aplicativo para a sua empresa </h2><p class="paragraph-card --blogIntegra"Não é novidade que os celulares e tablets estão sendo amplamente utilizados, tanto pela rapidez quanto pela facilidade de uso em qualquer lugar. Diante desse cenário, as empresas que tomam a consciência de que a sua marca também deve estar presente dentro desse mundo, conseguem se destacar aumentando a sua dinâmica de trabalho e também a sua receita. Veja outros benefícios que um aplicativo pode trazer:</p> <ul class="--blogIntegra"><li>- Aumento da comunicação com os clientes</li><li>- Facilidade na criação de canais de vendas</li><li>- Visibilidade da marca</li><li>- Implementação de campanhas e promoções</li><li>- Controle de gastos</li><li>- Criação de canal de dúvidas e sugestões</li></ul> <h2 class="subtitle-card --blogIntegra">Afinal, quanto custa um aplicativo?</h2> <p class="paragraph-card --blog">Para estimar quanto custa um aplicativo, as variáveis apresentadas devem ser consideradas, lembrando que outros fatores também podem ter peso na hora de estipular o valor final. Outro ponto importante ainda não citado e que também tem uma influência grande na hora da precificação é a interface, que dependendo da escolha, pode fazer com que o aplicativo assuma um valor bem maior do que o esperado. Para chegar a um valor médio, os preços que serão apresentados foram separados de acordo com o nível tecnológico utilizado e também o tipo de profissional que irá realizar o trabalho, sendo eles: <ul class="--blogIntegra"><li><strong>- Entre R$3.500,00 e R$6.000,00:</strong> geralmente o aplicativo não irá utilizar uma tecnologia tão avançada e a sua interface será simples.</li><li><strong>- Entre R$7.000,00 e R$15.000,00:</strong> o aplicativo irá apresentar uma qualidade maior e pode ter a finalidade de atender um público volumoso.</li><li><strong>- A partir de R$15.000,00:</strong> ele terá que ser desenvolvido por profissional altamente qualificado, com uma interface bastante inovadora e com a finalidade de atrair e atender milhões de usuários.</li></ul><p class="paragraph-card --blog">Agora que você já tem uma ideia de quanto custa um aplicativo, pense no modelo de negócio e veja o quão importante será o app para o usuário final. Diante de milhares de opções, se faz necessário que o aplicativo tenha uma função de grande relevância para que o usuário o mantenha instalado em seu dispositivo.</p>';
var cardsBlog = [
  {
    idBlog: 1,
    title: "Desenvolvimento de APP’s: Quanto custa um aplicativo?",
    date: "05/10/2021",
    singleHeroBanner: "assets/img/banner-hero_suporte-e-manutencao.png",
    singleImage: "assets/img/card-blog-1.png",
    post: "desenvolvimento-de-apps-quanto-custa-uma-aplicacao",
  },
  {
    idBlog: 2,
    title: "Por que meu blog não consegue aparecer no Google?",
    date: "04/10/2021",
    singleHeroBanner: "assets/img/banner-hero_suporte-e-manutencao.png",
    singleImage: "assets/img/card-blog-2.png",
    post: "por-que-meu-blog-nao-consegue-aparecer-no-google",
  },
  {
    idBlog: 3,
    title:
      "WordPress, Drupal, Shopify, Joomla ou Wix: Qual é a melhor plataforma para criação de conteúdo?",
    date: "04/10/2021",
    singleHeroBanner: "assets/img/banner-hero_suporte-e-manutencao.png",
    singleImage: "assets/img/card-blog-3.png",
    post: "wordpress-drupal-shopify-joomla-ou-wix-qual-e-a-melhor-plataforma-para-criacao-de-conteudo",
  },
  {
    idBlog: 4,
    title:
      "Responsividade: como e porquê ela é importante para o seu site/portal?",
    date: "03/10/2021",
    singleHeroBanner: "assets/img/banner-hero_suporte-e-manutencao.png",
    singleImage: "assets/img/card-blog-4.png",
    post: "responsividade-como-e-porque-ela-e-importante-para-o-seu-site-ou-portal",
  },
];

function createCardsPostBlog() {
  try {
    const cardsPostBlog = document.querySelector(".cards-post-blog");
    cardsPostBlog.innerHTML = cardsBlog
      .map(
        (cardBlog) => `
        <div class="card-post-blog --blog">
            <a href="blog-integra.php?post=${cardBlog.post}">
                <div style="background-image: url(${cardBlog.singleImage}" class="card-img --blog" >
                </div>
            </a>
            <div class="card-content --blog">
                <p>${cardBlog.date}</p>
                <h3> <a href="blog-integra.php?post=${cardBlog.post}" class="h3-anchor">
                ${cardBlog.title}
                </a></h3>
                <a href="blog-integra.php?post=${cardBlog.post}" class="anchor-blog">> leia mais</a>
            </div>
    </div>`
      )
      .join("");
  } catch (err) {
    console.log(err);
  }
}
const post = new URLSearchParams(document.location.search);

const cardBlogSlugFiltered = cardsBlog.filter(
  (cardBlog) => cardBlog.post == post.get("post")
);

function createBlogPage() {
  try {
    const cardBlogBannerHeroImg = document.querySelector(
      "#card-blog_banner-hero-img"
    );
    const cardBlogBannerHeroTitle = document.querySelector(
      "#card-blog_banner-hero-title"
    );
    const cardBlog = document.querySelector("#card_blog");

    cardBlogBannerHeroTitle.innerText = cardBlogSlugFiltered.map(
      (cardBlogFiltered) => `${cardBlogFiltered.title}`
    );
    cardBlogBannerHeroImg.style.backgroundImage = cardBlogSlugFiltered.map(
      (cardBlogFiltered) => `url("${cardBlogFiltered.singleHeroBanner}")`
    );

    cardBlog.innerHTML = cardBlogSlugFiltered.map(
      (cardBlogFiltered) => `
          <div class="photo-card --blog" style="background-image: url(${cardBlogFiltered.singleImage})"> <!--photo-card-->
          </div> <!--/photo-card-->
          <div class="info-card --blog"> 
              <div class="texto-card --blogIntegra">
                  <p>${textoApp}</p>
                  <div class="integra-share">
                    <h4>Compartilhe</h4>
                    <img src="assets/img/facebook-share.svg" class="integra-share-img" alt="Ícone da rede social Facebook">
                    <img src="assets/img/twitter-share.svg" class="integra-share-img" alt="Ícone da rede social Twitter">
                    <img src="assets/img/linkedin-share.svg" class="integra-share-img" alt="Ícone da rede social Linkedin">
                    <img src="assets/img/whatsapp-share.svg" class="integra-share-img" alt="Ícone da rede social Whatsapp">
                  </div>    
              </div>
          </div>
      `
    );
  } catch (err) {
    console.log(err);
  }
}

function createSingleBlogPosts() {
  try {
    const blogSinglePosts = document.querySelector(
      ".cards-post-blog.cards-other-posts"
    );
    blogSinglePosts.innerHTML = cardsBlog
      .map((cardBlog) =>
        cardBlog.post == post.get("post")
          ? ""
          : `<div class="card-post-blog --blog">
        <a href="blog-integra.php?post=${cardBlog.post}">
            <div style="background-image: url(${cardBlog.singleImage}" class="card-img --blog" >
            </div>
        </a>
        <div class="card-content --blog --blogIntegra">
            <p>${cardBlog.date}</p>
            <h3><a href="blog-integra.php?post=${cardBlog.post}" class="h3-anchor title-post-integra">
            ${cardBlog.title}
            </a></h3>
            <a href="blog-integra.php?post=${cardBlog.post}" class="anchor-blog">> leia mais</a>
        </div>
</div>
    `
      )
      .join("");
  } catch (err) {
    console.log(err);
  }
}
createBlogPage();
createCardsPostBlog();
createSingleBlogPosts();
