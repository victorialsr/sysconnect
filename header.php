<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="UTF-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <?php include_once("title-page.php"); ?>
  <title><?php echo $titulos[$uri];?></title>
  <link rel="shortcut icon" href="assets/img/favicon.ico" />
  <link rel="stylesheet" href="css/style.css" />
  <link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />
  <link rel="preconnect" href="https://fonts.googleapis.com" />
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;400;500;600;700;900&display=swap" rel="stylesheet" />
</head>

<body>
  <header>
    <ul class="menu-top">
      <a href="#">
        <li><img src="./assets/img/logo-linkedin.png" alt="Logo da rede social Linkedin"></li>
      </a>
      <a href="#">
        <li><img src="./assets/img/logo-twitter.png" alt="Logo da rede social Twitter"></li>
      </a>
      <a href="#">
        <li><img src="./assets/img/logo-facebook.png" alt="Logo da rede social Facebook"></li>
      </a>
      <a href="#">
        <li><img src="./assets/img/logo-youtube.png" alt="Logo da rede social Youtube"></li>
      </a>
    </ul>
    <nav class="menu">
      <a href="index.php" class="logo">
        <img src="./assets/img/logo.png" class="logo-img" alt="Ícone do logo da empresa Sysconnect" />
        <img src="./assets/img/sysconnect.png" class="logo-txt" alt="Texto do logo da empresa Sysconnect" />
      </a>
      <button id="btn-mobile">
        <span id="hamburguer"></span>
      </button>
      <ul class="menu-list">
        <li class="menu-link"><a href="quem-somos.php">Quem somos</a></li>
        <li class="menu-link"><a href="servicos.php">Serviços</a> </li>
        <li class="menu-link"><a href="blog.php">Blog</a> </li>
        <li class="menu-link"><a href="contato.php">Contato</a> </li>
      </ul>
    </nav>

  </header>