<?php require_once ("header.php"); ?>
<main class="main-servicos">
<section class="banner banner-hero --servicos"> <!--section-hero-->
        <div class="container-fluid container-banner --servicos container_banner-hero">
            <nav class="breadcrumb">
                <ul>
                    <li>Você está em</li>
                    <li><a class="breadcrumb-active" href="index.php" >Home</a></li>
                    <li><a href="servicos.php" class="breadcrumb-active"> Serviços</a></li>
                </ul>
            </nav>
        <h1 class="main-title">Serviços</h1> 
        </div> 
    </section> <!--/section-hero-->
    <section class="section-card --servicos">
        <div class="container container-mobile-fluid --servicos">
            <h3 class="subtitle-h3">Somos especialistas em desenvolvimento de soluções em ti</h3>
            <h2 class="title-h2">Conheça todos os nossos Serviços</h2>
            <div class="grid">
            </div>
        </div>
    </section>

    <section class="banner banner-cta --servicos"> <!--section-banner-cta-->
        <div class="container-fluid container-banner-cta">
            <div class="text-banner-cta"> 
                <h2 class="title-h2-banner --servicos">Gostou dos nossos serviços? Faça já seu orçamento Conosco!</h2>
            </div>  
            <a href="contato.php" class="btn btn-cta --servicos">quero um orçamento gratuito</a>
        </div>
    </div>
       
    </section> <!--/section-banner-cta-->
</main>
<?php require_once ("footer.php"); ?>