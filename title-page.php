<?php 
    $companyName = "Sysconnect";
    $nomeServico = '';
    $nomeBlog = '';

    if(empty($_GET['slug'])){
        $titulos = [
        'index.php' => $companyName,
        'quem-somos.php' => 'Quem Somos | ' . $companyName,
        'servicos.php' => 'Serviços | ' . $companyName,
        'blog.php' => 'Blog | ' . $companyName,
        'contato.php' => 'Contato | ' . $companyName,
        ];
    }
    else if (isset($_GET['slug'])){
    $slugServico = $_GET['slug'];
    switch ($slugServico) {
        case 'suporte-e-manutencao':
        $nomeServico = 'Suporte e Manutenção';
        break;
        case 'criacao-de-sites':
        $nomeServico = 'Criação de Sites';
        break;
        case 'sistemas-em-nuvem':
        $nomeServico = 'Sistemas em Nuvem';
        break;
        case 'ecommerce':
        $nomeServico = 'E-commerce';
        break;
        case 'seo':
        $nomeServico = 'SEO';
        break;
        case 'intranet-e-extranet':
        $nomeServico = 'Intranet e Extranet';
        break;
        case 'google-adwords':
        $nomeServico = 'Google Adwords';
        break;
        case 'criacao-de-apps':
        $nomeServico = 'Criação de Apps';
        break;   
        case 'marketing-de-conteudo':
        $nomeServico = 'Marketing de Conteúdo';
        break;            
        }
        $titulos = ['servicos-integra.php' => $nomeServico. ' | Serviços | ' . $companyName];
    }
        if (isset($_GET['post'])){
        $post = $_GET['post'];
        switch ($post) {
        case 'desenvolvimento-de-apps-quanto-custa-uma-aplicacao':
        $nomeBlog = 'Desenvolvimento de APP’s: Quanto custa um aplicativo?';
        break;
        case 'por-que-meu-blog-nao-consegue-aparecer-no-google':
        $nomeBlog = 'Por que meu blog não consegue aparecer no Google?';
        break;
        case 'wordpress-drupal-shopify-joomla-ou-wix-qual-e-a-melhor-plataforma-para-criacao-de-conteudo':
        $nomeBlog = 'WordPress, Drupal, Shopify, Joomla ou Wix: Qual é a melhor plataforma para criação de conteúdo?';
        break;
        case 'responsividade-como-e-porque-ela-e-importante-para-o-seu-site-ou-portal':
        $nomeBlog = 'Responsividade: como e porquê ela é importante para o seu site/portal?';
        break;
        }
        $titulos = ['blog-integra.php' => $nomeBlog. ' | Blog | ' . $companyName];
    } 
    $exp = explode("/", $_SERVER['PHP_SELF']);
    $uri = end($exp);
?>