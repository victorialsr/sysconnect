<footer>
	<div class="container container-footer">
	  <div class="footer-row">
		<div class="footer-col-1">
		  <ul>
			<li class="footer-item large"><a href="quem-somos.php">Quem somos</a></li>
			<li class="footer-item large"><a href="servicos.php">Serviços</a></li>
			<li class="footer-item"><a href="servicos-integra.php?slug=suporte-e-manutencao">Suporte e manutenção</a></li>
			<li class="footer-item"><a href="servicos-integra.php?slug=criacao-de-sites">Criação de sites</a></li>
			<li class="footer-item"><a href="servicos-integra.php?slug=sistemas-em-nuvem">Sistemas em Nuvem</a></li>
			<li class="footer-item"><a href="servicos-integra.php?slug=ecommerce">E-commerce</a></li>
			<li class="footer-item"><a href="servicos-integra.php?slug=seo">SEO</a></li>
			<li class="footer-item"><a href="servicos-integra.php?slug=intranet-e-extranet">Intranet e Extranet</a></li>
			<li class="footer-item"><a href="servicos-integra.php?slug=google-adwords">Google Adwords</a></li>
			<li class="footer-item"><a href="servicos-integra.php?slug=criacao-de-apps">Criação de App's</a></li>
			<li class="footer-item"><a href="servicos-integra.php?slug=marketing-de-conteudo">Marketing de Conteúdo</a></li>
		  </ul>
		</div>

		<div class="footer-col-2">
		  <ul>
			
			<li class="footer-item large"><a href="blog.php">Blog</a></li>
			
			<li class="footer-item large"><a href="contato.php">Contato</a></li>
		  </ul>
		</div>
	  </div>

	  <div class="footer-col-3">
		<h4 class="footer-contact-title">Siga-nos:</h4>
		<ul class="social-media-footer">
		  <a href="#">
			<li><img src="./assets/img/logo-linkedin.png" alt="Logo da rede social Linkedin"></li>
		  </a>
		  <a href="#">
		  <li><img src="./assets/img/logo-twitter.png" alt="Logo da rede social Twitter"></li>
		  </a>
		  <a href="#">
		  <li><img src="./assets/img/logo-facebook.png" alt="Logo da rede social Facebook"></li>
		  </a>
		  <a href="#">
		  <li><img src="./assets/img/logo-youtube.png" alt="Logo da rede social Youtube"></li>
		  </a>
		</ul>
		<h4 class="footer-contact-title">Telefone:</h4>
		<a href="tel:+550012345678" class="footer-contact-anchor">(00) 1234-5678</a>
		<h4 class="footer-contact-title">E-mail</h4>
		<a href="mailto:contato@sysconnect.com" class="footer-contact-anchor">contato@sysconnect.com</a>
	  </div>
	</div>
	<div class="copyright-footer">
	  <div class="container">
			<div class="teste">
				<p>2021 - Todos os direitos reservados.</p>
				<div class="copyright-footer-kbrtec">
					<p>Desenvolvido por</p> 
					<a href="https://www.kbrtec.com.br"><img src="./assets/img/kbrtec-logo-footer.png" alt="Logo da empresa KBR Tec"></a>
		  	</div>
			</div>
		</div>
	</div>
</footer>
<script src="js/services.js"></script>
<script src="js/blog.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>
<script type="text/javascript">
    $("#telefone").mask("(00) 0000-00009");
</script>
<script src="js/script.js"></script>
  
</body>

</html>