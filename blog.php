<?php require_once ("header.php"); ?>

<main class="main-blog">
    <section class="banner banner-hero --blog"> <!--section-hero-->
        <div class="container-fluid container-banner-hero">
            <nav class="breadcrumb">
                <ul>
                    <li>Você está em</li>
                    <li><a class="breadcrumb-active" href="index.php">Home </a></li>
                    <li><a href="blog.php" class="breadcrumb-active">Blog</a></li>
                </ul>
            </nav>
            <h1 class="main-title">Blog</h1>
        </div>            
    </section> <!--/section-hero-->

   
    <section class="section-card-blog --blog">
        <div class="container container-card-blog --blog">
            <h3 class="subtitle-h3">Últimas notícias sobre</h3>
            <h2 class="title-h2 --blog">Tecnologia, criação de sites, aplicativos e sistemas</h2>
            <div class="cards-post-blog">
                
            </div>
        </div>
    </section> 

 
    <section class="banner banner-cta --blog"> <!--section-banner-cta-->
        <div class="container-fluid container-banner-cta">
            <div class="text-banner-cta --quemSomos"> 
                <h2 class="title-h2-banner --quemSomos">A SOLUÇÃO IDEAL VOCÊ ENCONTRA AQUI!</h2>
            </div>  
            <a href="contato.php" class="btn btn-cta --quemSomos">quero um orçamento gratuito</a>
        </div>
    </section> <!--/section-banner-cta-->
</main>
<?php require_once ("footer.php"); ?>