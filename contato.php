<?php require_once ("header.php");?>

<main class="main-contato">
    <section class="banner banner-hero --contato"> <!--section-hero-->
        <div class="container-fluid container-banner-hero">
            <nav class="breadcrumb">
                <ul>
                    <li>Você está em</li>
                    <li><a class="breadcrumb-active" href="index.php">Home </a></li>
                    <li><a href="contato.php" class="breadcrumb-active"> Contato</a></li>
                </ul>
            </nav>
            <h1 class="main-title">Contato</h1>
        </div>            
    </section> <!--/section-hero-->
   
    <section class="section-info-card --contato">  
    <?php
        if(!isset($_POST['enviar'])):
    ?>
        <div class="container-fluid container-mobile-fluid container-card --contato">
            <div class="contact-text">
                <div class="quadrado-fale-conosco"></div>
                <h2 class="title title-contato">Fale conosco</h2> 
                <p>Preencha o formulário ao lado que em breve entraremos em contato.</p>
            </div>
            <div class="card --contato">
                <div class="info-card --contato">
                    <div class="texto-card --contato">
                        <form action="#" method="post">
                            <div>
                            <label for="name">Seu nome:</label>
                            <input type="text" id="nome" name="nome" minlength="3" required pattern="[A-Za-z\d](?:[A-Za-z\d]| (?! |$)){0,29}" />
                            </div>
                            <div class="row-contact">
                                <div class="mail-area">
                                    <label for="email">Seu e-mail:</label>
                                    <input type="email" id="email" placeholder="Ex: mail@exemplo.com.br" name="email"required/>
                                </div>
                                <div class="tel-area">
                                    <label for="tel">Telefone:</label>
                                    <input type="tel" id="telefone" placeholder="Ex: 13 997586542" required minlength="10" name="telefone" pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}"/>
                                </div>
                            </div>
                            <div>
                                <label for="assunto">Assunto:</label>
                                <input id="assunto" placeholder="Ex: Criação de Sites" required name="assunto" minlenth="4" required pattern="[A-Za-z\d](?:[A-Za-z\d]| (?! |$)){0,29}"></input>
                            </div>
                            <div>
                                <label for="mensagem">Mensagem:</label>
                                <textarea id="mensagem" placeholder="Digite aqui a mensagem que deseja nos passar" name="mensagem" required required pattern="[A-Za-z\d](?:[A-Za-z\d]| (?! |$)){0,29}" minlength="15"></textarea>
                            </div>
                            <button class="btn --contato btn-submit" type="submit" name="enviar" id="enviar"> Enviar</button>
                        </form>
                    </div>                 
                </div> 
                    </div>                 
                </div> 
            </div>
                </div> 
                    </div>                 
        </div>
                    </div>                 
                </div>
            </div>
        <?php else: ?>        
            <div id="form-action">
                <div class="card-form-action-message">
                    <div class="form-action-message">
                        <img src="./assets/img/checked-icon.svg" alt="Ícone de check, simbolizando que a mensagem foi enviada com sucesso.">
                        <p>Olá, <span><?=$_POST['nome']?></span>!</p>
                        <p>Recebemos o seu contato sobre <span><?=$_POST['assunto']?></span>. Assim que possível retornaremos através do email <span><?=$_POST['email'];?></span> ou pelo telefone <span><?=$_POST['telefone']?></span>. Se desejar entre em contato conosco pelos nossos outros contatos abaixo, ou conheça todos os nossos serviços. </p>
                        <a href="servicos.php" class="btn btn-card">Nossos serviços</a>   
                    </div>
                </div>
            </div>
        </div>
        <?php endif ?>    
    </section>             
    <section class="section-statistic-data --contato"> <!--section-statistic-data-->
        <div class="container container-statistic-data --contato">
        <h3 class="subtitle-h3">se desejar nos contate também em</h3>
        <h2 class="title-h2">nossos outros contatos</h2>
            <div class="statistic-data --contato"> 
                <div class="col-statistic-data --contato"> 
                    <div class="border-statistic-data --contato">
                        <div class="statistic-data-number --contato --whatsapp">
                            <a href="https://api.whatsapp.com/send?phone=55139999999999"><img src="./assets/img/logo-whatsapp.png" alt="Ícone do aplicativo Whatsapp"></a>

                        </div> 
                    </div>
                    <div class="statistic-description --contato">
                        <p class="statistic-paragraph">Whatsapp: </p>
                        <a href="https://api.whatsapp.com/send?phone=55139999999999">(13) 9 999-9999</a>
                    </div>
                </div> 
                <div class="col-statistic-data --contato"> <!--fourth data-->
                    <div class="border-statistic-data --contato">
                        <div class="statistic-data-number --contato --telephone">
                        <a href="tel:+551333333333">
                            <img src="./assets/img/logo-telephone.png" alt="Ícone de telefone"></a>
                        </div> 
                    </div>
                    <div class="statistic-description --contato">
                        <p class="statistic-paragraph">Telefone</p>
                        <a href="tel:+551333333333">(13) 3333-3333</a>
                    </div>
                </div> 
                <div class="col-statistic-data --contato"> <!--fourth data-->
                    <div class="border-statistic-data --contato">
                        <div class="statistic-data-number --contato --email">
                            <a href="mailto:contato@sysconnect.com">
                                <img src="./assets/img/logo-email.png" alt="Ícone de um envelope, simbolizando e-mail">
                            </a>
                        </div> 
                    </div>
                    <div class="statistic-description --contato">
                        <p class="statistic-paragraph">E-mail</p>
                        <a href="mailto:contato@sysconnect.com">contato@sysconnect.com</a>
                    </div>
                </div> 
            </div>
            
        </div>
    </section> <!--/section-statistic-data-->

    <section class="section-contact-location --quemSomos"> <!--section-statistic-data-->
        <div class="container-fluid container-statistic-data --quemSomos">
      <h2 class="title-h2">Onde nos encontrar</h2>  
            <div class="mapa">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3646.234469685566!2d-46.3299631!3d-23.9521476!3m2!1i1024!2i768!4f13.1!3m3!3m2!1spt-BR!2sbr!4v1635523560756!5m2!1spt-BR!2sbr" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </iframe> 
                <div class="retangulo-mapa">
                    <img src="assets/img/map-flag.png" alt="Símbolo de ponteiro vermelho de mapa">  
                    <div class="texto-mapa">  
                        <a href="https://g.page/kbrtec?share">
                        <h3>ENDEREÇO</h3>
                        <address>Av. Senador Feijó, 686 - Sala 1625</address>
                    <address>Vila Mathias - Santos / SP</address>
                    <address>CEP: 11015-504</address>
                   </a>
                </div>
            </div>
        </div>
    </section> <!--/section-statistic-data-->
    <section class="banner banner-cta --quemSomos"> <!--section-banner-cta-->
        <div class="container-fluid container-banner-cta">
            <div class="text-banner-cta --quemSomos"> 
                <h2 class="title-h2-banner --quemSomos">A SOLUÇÃO IDEAL VOCÊ ENCONTRA AQUI!</h2>
            </div>  
            <a href="contato.php" class="btn btn-cta --quemSomos">quero um orçamento gratuito</a>
        </div>
    </section> <!--/section-banner-cta-->
</main>
<?php require_once ("footer.php"); ?>