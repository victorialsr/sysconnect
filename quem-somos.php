<?php require_once ("header.php"); ?>
<main class="quem-somos">
    <section class="banner banner-hero --quemSomos"> <!--section-hero-->
        <div class="container-fluid container-banner-hero --quemSomos">
            <nav class="breadcrumb">
                <ul>
                    <li>Você está em</li>
                    <li><a class="breadcrumb-active" href="index.php">Home</a></li>
                    <li><a class="breadcrumb-active" href="#"> Quem somos</a></li>
                </ul>
            </nav>
            <h1 class="main-title">Quem Somos</h1> 
        </div>            
    </section> <!--/section-hero-->
   
    <section class="section-info-card --quemSomos"> <!--section-card-->
        <div class="container container-mobile-fluid container-card --quemSomos">
            <div class="card">
                <div class="info-card"> <!--info-card-->
                    <div class="texto-card">
                        <h3 class="subtitle-card">bem-vindo a nossa empresa</h2>
                        <h2 class="title-card --quemSomos">UM RESUMO SOBRE A SYSCONNECT</h2>
                        <p class="paragraph-card --quemSomos">A Sysconnect nasceu da vontade de fornecer aos seus clientes serviços que possam alavancar seus negócios através da tecnologia, fornecendo soluções que estejam de acordo com aquilo que eles esperam e o que o mercado necessita.</p>
                        <p class="paragraph-card --quemSomos">Nosso maior desafio é materializar boas ideias em soluções de sucesso - práticas e simples, com foco total em resultados. Este é o nosso trabalho e esta é a nossa paixão!</p>
                        <p class="paragraph-card --quemSomos">Nosso time é muito experiente e 100% focado em entregar ações de grande impacto, com resultados mensuráveis aos nossos clientes. Oferecemos design moderno com tecnologia de ponta. Atenção total ao seu posicionamento no Google e performance mobile (celulares e tablets).</p>
                    </div>
                    <a href="contato.php" class="btn btn-card --quemSomos">Dúvida? Fale conosco</a>
                </div> <!--/info-card-->
                <div class="photo-card --quemSomos"> <!--photo-card-->
                </div> <!--/photo-card-->
            </div>
        </div>
    </section> <!--/section-card-->
    
    <section class="section-statistic-data --quemSomos"> <!--section-statistic-data-->
        <div class="container container-statistic-data --quemSomos">
        <h3 class="subtitle-h3">por que somos diferentes?</h3>
        <h2 class="title-h2">nossos resultados são baseados em dados</h2>
            <div class="statistic-data --quemSomos"> 
                <div class="col-statistic-data --quemSomos"> <!--first data-->
                    <div class="border-statistic-data --quemSomos">
                        <div class="statistic-data-number --quemSomos">
                            <p><span>8</span></p>
                        </div>
                    </div>
                    <div class="statistic-description --quemSomos">
                        <p class="statistic-paragraph">Estados</p>
                        <p class="statistic-paragraph">brasileiros</p>
                    </div>
                </div> <!--/first data-->
                
                <div class="col-statistic-data --quemSomos"> <!--second data-->
                    <div class="border-statistic-data --quemSomos">              
                        <div class="statistic-data-number --quemSomos">
                            <p><span>15</span></p>
                        </div>
                    </div> 
                    <div class="statistic-description --quemSomos">
                        <p class="statistic-paragraph">Anos </p>
                        <p class="statistic-paragraph">de Mercado<p>
                    </div>
                </div> <!--/second data-->

                <div class="col-statistic-data --quemSomos"> <!--third data-->
                    <div class="border-statistic-data --quemSomos">
                        <div class="statistic-data-number --quemSomos">
                                <p><span>800</span></p>
                        </div>
                    </div>  
                    <div class="statistic-description --quemSomos">
                        <p class="statistic-paragraph">Projetos</p>
                        <p class="statistic-paragraph">Desenvolvidos</p>
                    </div>
                </div> <!--/third data-->

                <div class="col-statistic-data --quemSomos"> <!--fourth data-->
                    <div class="border-statistic-data --quemSomos">
                        <div class="statistic-data-number --quemSomos statistic-data-google">
                            <img src="./assets/img/img-statistic-google.png" alt="Logo do selo Google Partner">
                        </div> 
                    </div>
                    <div class="statistic-description --quemSomos">
                        <p class="statistic-paragraph">Selo</p>
                        <p class="statistic-paragraph">Google Partner</p>
                    </div>
                </div> <!--/fourth data-->
            </div>
        </div>
    </section> <!--/section-statistic-data-->
    
    <section class="banner banner-cta --quemSomos"> <!--section-banner-cta-->
        <div class="container-fluid container-banner-cta">
            <div class="text-banner-cta --quemSomos"> 
                <h2 class="title-h2-banner --quemSomos">A SOLUÇÃO IDEAL VOCÊ ENCONTRA AQUI!</h2>
            </div>  
            <a href="contato.php" class="btn btn-cta --quemSomos">quero um orçamento gratuito</a>
        </div>
    </section> <!--/section-banner-cta-->
</main>
<?php require_once ("footer.php"); ?>